## Affichez les infos des cartes réseau de votre PC :
## Affichez votre gateway:
``` 
C:\WINDOWS\system32>ipconfig

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::31dd:1ae5:ce8f:fcae%14
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.156
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253

Carte Ethernet Ethernet 2 :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::410:f753:7297:916f%4
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
```
## Trouvez comment afficher les informations sur une carte IP :
```
Suffixe DNS propre à la connexion: auvence.co

Mac : Adresse physique: ‎08-D2-3E-62-C0-DC

IP : Adresse IPv4: 10.33.2.156

Passerelle : Passerelle par défaut IPv4: 10.33.3.253

```
### A quoi sert la gateway dans le réseau d'YNOV ?

La gateway du réseau d'Ynov

## Utilisez l'interface graphique de votre OS pour changer d'adresse IP :
```
C:\WINDOWS\system32>ping 10.33.2.8

Envoi d’une requête 'Ping'  10.33.2.8 avec 32 octets de données :
Réponse de 10.33.2.8 : octets=32 temps=58 ms TTL=64
```
### Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération ?
Il est possible de perdre l'accées internets car au changement de l'Ip le serveur Dns n'est plus donner automatiquement, donc sans le serveur Dns nous ne pouvons pas avoir accés à internet.

##  Et si on remplissait un peu la table ?
```
10.33.2.8             a4-5e-60-ed-0b-27     dynamique
10.33.1.76            e0-cc-f8-7a-4f-4a     dynamique
10.33.1.103           18-65-90-ce-f5-63     dynamique
```
## Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre
```
C:\WINDOWS\system32>nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:18 Paris, Madrid (heure dÆÚtÚ)
Stats: 0:00:17 elapsed; 0 hosts completed (0 up), 1023 undergoing ARP Ping Scan
ARP Ping Scan Timing: About 81.43% done; ETC: 09:18 (0:00:04 remaining)
Nmap scan report for 10.33.0.6
Host is up (0.083s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
```
## Nmap pour touver une adresse ip Libre :

```
Nmap scan report for 10.33.3.249
Host is up (0.20s latency).
MAC Address: 58:96:1D:17:43:F5 (Intel Corporate)
Nmap scan report for 10.33.3.252
```
## Changement de l'adresse ip :
```
Carte réseau sans fil Wi-Fi :
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.250
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```
### RELAX
![](https://data.photofunky.net/output/image/d/c/0/4/dc04e9/photofunky.gif)

# 5. Petit chat privé

```
netcat, écoute sur le port numéro 8888 stp
netcat, connecte toi au port 8888 de la machine 192.168.196.22 stp
:)
```
### Pour aller un peu plus loin
J'écoute uniquement sur mon ethernet.
```
C:\WINDOWS\system32>nc.exe -l -p 9999 192.168.56.1
```
A partir d'ici je l'ai fait chez moi. :)
## Exploration du DHCP, depuis votre PC

```
C:\WINDOWS\system32>ipconfig/all

Serveur DHCP . . . . . . . . . . . . . : 192.168.1.254

Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 09:40:23
   Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 07:51:17
```
## Trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.254
```

##  Utiliser, en ligne de commande l'outil nslookup
```
C:\WINDOWS\system32>nslookup

> google.com
Serveur :   UnKnown
Address:  192.168.1.254

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:810::200e
          216.58.215.46
          
> ynov.com
Serveur :   UnKnown
Address:  192.168.1.254

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143

> 78.74.21.21
Serveur :   UnKnown
Address:  192.168.1.254

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

> 92.146.54.88
Serveur :   UnKnown
Address:  192.168.1.254

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```