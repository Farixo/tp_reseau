# TP3 : Progressons vers le réseau d'infrastructure

# Sommaire

- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [Sommaire](#sommaire)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
  - [1. Adressage](#1-adressage)
  - [2. Routeur](#2-routeur)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [A. Our own DNS server](#a-our-own-dns-server)
    - [B. SETUP copain](#b-setup-copain)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
- [Entracte](#entracte)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [A. L'introduction wola](#a-lintroduction-wola)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie--tcp-et-udp)
- [V. El final](#v-el-final)

# I. (mini)Architecture réseau

**Le but ici va être de mettre en place 3 réseaux, dont vous choisirez vous-mêmes les adresses.** Les contraintes :

- chaque réseau doit commencer par `10.3.`
  - `10` -> adresse privée
  - `3` -> c'est notre troisième TP :)
- chaque réseau doit être **le plus petit possible** : choisissez judicieusement vos masques
- aucun réseau ne doit se superposer (pas de doublons d'adresses IP possible)
- il y aura
  - deux réseaux pour des serveurs
  - un réseau pour des clients
- en terme de taille de réseau, vous compterez
  - 35 clients max pour le réseau `client1`
  - 63 serveurs max pour le réseau `server1`
  - 10 serveurs max pour le réseau `server2`
- les réseaux doivent, autant que possible, se suivre
- [référez-vous au cours dédié au subnetting](../../cours/ip/README.md#2-subnetting) si vous ne savez pas comment vous y prendre



---

**Il y aura un routeur dans le TP**, qui acheminera les paquets d'un réseau à l'autre. Ce routeur sera donc la **passerelle** des 3 réseaux.  
On peut aussi dire qu'il sera la passerelle des machines, dans chacun des réseaux.  

> *Cette machine aura donc une carte réseau dans TOUS les réseaux.*

**Pour ce routeur, vous respecterez la convention qui consiste à lui attribuer la dernière IP disponible du réseau dans lequel il se trouve.**

> *Par exemple, dans le réseau `192.168.10.0/24`, par convention, on définira sur le routeur l'adresse IP `192.168.10.254`.*

## 1. Adressage

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.1.128/26`        | `255.255.255.192` | 64                           | `10.3.1.190`         | `10.3.1.191`                                                                                   |
| `server1`     | `10.3.1.0/25`        | `255.255.255.128` | 128                           | `10.3.1.126`         | `10.3.1.127`                                                                                   |
| `server2`     | `10.3.1.192/28`        | `255.255.255.240` | 16                           | `10.3.1.206`         | `10.3.1.207`                                                                                   |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.1.190/26`      |`10.3.1.126/25`       | `10.3.1.206/28`      | Carte NAT             |
| ...          | ...                  | ...                  | ...                  | `10.3.?.?/?`          |

> *N'oubliez pas de **TOUJOURS** fournir le masque quand vous écrivez une IP.*

## 2. Routeur

🖥️ **VM router.tp3**

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:11:0c:21 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe11:c21/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7b:0f:af brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe7b:faf/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:92:43:75 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe92:4375/64 scope link
       valid_lft forever preferred_lft forever
```
```
[farixo@router ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=18.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=24.5 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.507/21.523/24.540/3.020 ms
```
```
[farixo@router ~]$ dig ynov.com

ynov.com.               4304    IN      A       92.243.16.143
```
```
[farixo@router ~]$ hostname
router.tp3
```
```
[farixo@router ~]$ sudo firewall-cmd --get-active-zone
[sudo] password for farixo:
public
  interfaces: enp0s3 enp0s8 enp0s9 enp0s10
[farixo@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
# II. Services d'infra

## 1. Serveur DHCP

🖥️ **VM `dhcp.client1.tp3`**

[farixo@dhcp ~]$ hostname
dhcp.client1.tp3

---

🌞 **Mettre en place un client dans le réseau `client1`**

- la machine récupérera une IP dynamiquement grâce au serveur DHCP

```
[admin@marcel ~]$ sudo nmcli con up enp0s8
[sudo] password for admin:
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)
```
```
[admin@marcel ~]$ ip a
{...}
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e5:c3:8f brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.130/26 brd 10.3.0.191 scope global dynamic noprefixroute enp0s8
       valid_lft 484sec preferred_lft 484sec
    inet6 fe80::a00:27ff:fee5:c38f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
- Ainsi que sa passerelle et une adresse d'un DNS utilisable

```
[admin@marcel ~]$ ip r s
default via 10.3.0.190 dev enp0s8 proto dhcp metric 100
10.3.0.128/26 dev enp0s8 proto kernel scope link src 10.3.0.130 metric 100
```
```
[admin@marcel ~]$ dig google.com

{...}
;; ANSWER SECTION:
google.com.             276     IN      A       216.58.215.46

;; Query time: 25 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Oct 17 21:11:42 CEST 2021
;; MSG SIZE  rcvd: 55
```

🌞 **Depuis `marcel.client1.tp3`**

- Prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP

```
[admin@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=26.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=18.3 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=31.2 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3009ms
rtt min/avg/max/mdev = 18.319/24.525/31.180/4.722 ms
```

```
[admin@marcel ~]$ dig google.com

{...}
;; ANSWER SECTION:
google.com.             276     IN      A       216.58.215.46

;; Query time: 25 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Oct 17 21:11:42 CEST 2021
;; MSG SIZE  rcvd: 55
```


- A l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau

```
[admin@marcel ~]$ traceroute google.com
traceroute to google.com (216.58.206.238), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  17.949 ms  16.224 ms  15.086 ms
 2  10.0.2.2 (10.0.2.2)  12.056 ms  11.808 ms  11.048 ms
 {...}
```

## 2. Serveur DNS

### B. SETUP copain

🌞 **Mettre en place une machine qui fera office de serveur DNS**

**Voir le fichier named.conf**

🌞 **Tester le DNS depuis `marcel.client1.tp3`**

- Essayez une résolution de nom avec `dig`
```
[admin@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4071
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: fc035919da0fe8f95ecd099f616c8db878c75f409de6b1e4 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       172.217.22.142

;; AUTHORITY SECTION:
google.com.             170742  IN      NS      ns2.google.com.
google.com.             170742  IN      NS      ns3.google.com.
google.com.             170742  IN      NS      ns1.google.com.
google.com.             170742  IN      NS      ns4.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         170742  IN      A       216.239.34.10
ns1.google.com.         170742  IN      A       216.239.32.10
ns3.google.com.         170742  IN      A       216.239.36.10
ns4.google.com.         170742  IN      A       216.239.38.10
ns2.google.com.         170742  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         170742  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         170742  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         170742  IN      AAAA    2001:4860:4802:38::a

;; Query time: 30 msec
;; SERVER: 10.3.0.11#53(10.3.0.11)
;; WHEN: Sun Oct 17 22:55:21 CEST 2021
;; MSG SIZE  rcvd: 331
```

- Prouvez que c'est bien votre serveur DNS qui répond pour chaque `dig`

```
[admin@marcel ~]$ dig google.com @10.3.0.11

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com @10.3.0.11
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 44922
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: b31af136226f3d5a062f4adf616c8b6a1bbda36e02fa8c23 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             183     IN      A       172.217.22.142

;; AUTHORITY SECTION:
google.com.             171332  IN      NS      ns1.google.com.
google.com.             171332  IN      NS      ns4.google.com.
google.com.             171332  IN      NS      ns2.google.com.
google.com.             171332  IN      NS      ns3.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         171332  IN      A       216.239.34.10
ns1.google.com.         171332  IN      A       216.239.32.10
ns3.google.com.         171332  IN      A       216.239.36.10
ns4.google.com.         171332  IN      A       216.239.38.10
ns2.google.com.         171332  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         171332  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         171332  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         171332  IN      AAAA    2001:4860:4802:38::a

;; Query time: 3 msec
;; SERVER: 10.3.0.11#53(10.3.0.11)
;; WHEN: Sun Oct 17 22:45:31 CEST 2021
;; MSG SIZE  rcvd: 331
```
on peut voir aussi a la 3e ligne avant la fin que c'est bien mon server dns qui repond

Par exemple, vous devriez pouvoir exécuter les commandes suivantes :

```bash
$ dig marcel.client1.tp3
$ dig dhcp.client1.tp3
```


🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

- Les serveurs, on le fait à la main
```
[admin@web1] sudo /etc/resolv.conf
nameserver 10.3.0.11
```

- Les clients, c'est fait *via* DHCP

**Voir le  fichier dhcpd.conf**


## 3. Get deeper

On va affiner un peu la configuration des outils mis en place.

### A. DNS forwarder

🌞 Test !

- vérifier depuis `marcel.client1.tp3` que vous pouvez résoudre des noms publics comme `google.com` en utilisant votre propre serveur DNS (commande `dig`)
- pour que ça fonctionne, il faut que `dns1.server1.tp3` soit lui-même capable de résoudre des noms, avec `1.1.1.1` par exemple

### B. On revient sur la conf du DHCP

🌞 **Affiner la configuration du DHCP**

- faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
```
[admin@dns1 ~]$ sudo cat /etc/named.conf
{...}
        recursion yes;
{...}
```

---

# III. Services métier

## 1. Serveur Web

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

```
[admin@web1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

```


🌞 **Test test test et re-test**

- testez que votre serveur web est accessible depuis `marcel.client1.tp3`
  - utilisez la commande `curl` pour effectuer des requêtes HTTP
```
[admin@marcel ~]$ curl 10.3.0.194
curl: (7) Failed to connect to 10.3.0.194 port 80: No route to host
[admin@marcel ~]$ curl 10.3.0.194:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

---

## 2. Partage de fichiers

### B. Le setup wola

🌞 **Setup d'une nouvelle machine, qui sera un serveur NFS**

```
[admin@nfs1 ~]$ sudo dnf -y install nfs-utils
[admin@nfs1 ~]$ sudo nano /etc/idmapd.conf
[admin@nfs1 ~]$ sudo nano /etc/exports
[admin@nfs1 ~]$ sudo mkdir /srv/nfs_share
[admin@nfs1 srv]$ sudo systemctl enable rpcbind nfs-server
[sudo] password for admin:
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[admin@nfs1 srv]$ sudo firewall-cmd --add-service=nfs --permanent
success
```

🌞 **Configuration du client NFS**

```
[admin@web1 system]$ sudo dnf -y install nfs-utils
[admin@web1 system]$ sudo nano /etc/idmapd.conf
[admin@web1 srv]$ sudo mount -t nfs 10.3.0.195:/srv/nfs_share /srv/nfs
```

🌞 **TEEEEST**

```
[admin@nfs1 ~]$ sudo touch /srv/nfs/test2
[admin@web1 ~]$ sudo ls -la /srv/nfs/
total 0
drwxrw-rw-. 2 root root 31  3 oct.  19:52 .
drwxr-xr-x. 3 root root 17  3 oct.  18:54 ..
-rw-r--r--. 1 root root  0  3 oct.  18:35 test
-rw-r--r--. 1 root root  0  3 oct.  19:52 test2
```

# IV. Un peu de théorie : TCP et UDP

Bon bah avec tous ces services, on a de la matière pour bosser sur TCP et UDP. P'tite partie technique pure avant de conclure.

🌞 **Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**

- SSH
- HTTP
- DNS
- NFS

📁 **Captures réseau `tp3_ssh.pcap`, `tp3_http.pcap`, `tp3_dns.pcap` et `tp3_nfs.pcap`**

> **Prenez le temps** de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.

🌞 **Expliquez-moi pourquoi je ne pose pas la question pour DHCP.**

🌞 **Capturez et mettez en évidence un *3-way handshake***

📁 **Capture réseau `tp3_3way.pcap`**

# V. El final

🌞 **Bah j'veux un schéma.**

- réalisé avec l'outil de votre choix
- un schéma clair qui représente
  - les réseaux
    - les adresses de réseau devront être visibles
  - toutes les machines, avec leurs noms
  - devront figurer les IPs de toutes les interfaces réseau du schéma
  - pour les serveurs : une indication de quel port est ouvert
- vous représenterez les host-only comme des switches
- dans le rendu, mettez moi ici à la fin :
  - le schéma
  - le 🗃️ tableau des réseaux 🗃️
  - le 🗃️ tableau d'adressage 🗃️
    - on appelle ça aussi un "plan d'adressage IP" :)

> J'vous le dis direct, un schéma moche avec Paint c'est -5 Points. Je vous recommande [draw.io](http://draw.io).

🌞 **Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**

- 📁 Fichiers de zone
- 📁 Fichier de conf principal DNS `named.conf`
- faites ça à peu près propre dans le rendu, que j'ai plus qu'à cliquer pour arriver sur le fichier ce serait top
